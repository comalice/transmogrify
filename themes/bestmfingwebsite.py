from collections import namedtuple
import datetime

'''
Atom <- filepath + metadata + content
Page <- Atom+
'''

Atom = namedtuple("Atom", ['path', 'text'])

class Theme():
    def __init__(self):
        self.title = "Best MFing Website"
        self.header = ""
        self.styling = ""
        self.menu_bar = ""
        self.footer = f"Copyright {datetime.datetime.now()}"

    def _list(self, items):
        print(items)
        _out = ""
        for item in items:
            _out = _out + "<li>" + item.text + r"<\li>" + "\n"
        print(_out)
        return "<ul>\n" + _out + r"\n<\ul>"

    def index(self, content):
        return self.page(self._list(content))

    def page(self, content):
        return self.html_doc(self.header + content + self.footer)

    def html_doc(self, content):
        return f"""<!DOCTYPE html>
<html lang="en">
<meta charset="utf8">
<meta name="viewport" content="width-device-width, initial-scale=1">
<title>{self.title}</title>
""" + """
<style>
  @media (prefers-color-scheme: dark){
    body {color:#fff;background:#000}
    a:link {color:#cdf}
    a:hover, a:visited:hover {color:#def}
    a:visited {color:#dcf}
  }
  body{
    margin:1em auto;
    max-width:40em;
    padding:0 .62em;
    font:1.2em/1.62 sans-serif
  }
  h1,h2,h3 {
    line-height:1.2
  }
  @media print{
    body{
      max-width:none
    }
  }
</style>
""" + f"""
{content}
<html>
"""
