+++
Title = "`.gitignore` in utf-16LE Won't be recognized by `git`"
Date = 2023/03/10
+++

I develop on Windows part of the time, and this means I trust `powershell` with some file creation tasks. On multiple occasions I've created a `.gitignore` file and it hasn't appeared to work. That is, files that should be ignored (for example `__pycache__/` folders) will persist when I use `git status`. The solution is simple, open the file in `code` and change the encoding to `UTF8`.