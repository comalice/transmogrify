import markdown
import os
from pathlib import Path

from themes.bestmfingwebsite import Theme, Atom
t = Theme()

public_path = "public"
index_path = "index.html"
content = "content"

# Load content and preserve filepaths.
pages = []
for root, dirs, files in os.walk(Path(content)):
    path = root.split(os.sep)
    #print((len(path) - 1) * "---", os.path.basename(root))
    for f in files:
        p = Path(root)/Path(f).__str__()
        with open(Path(root) / Path(f)) as fc:
            pages.append(Atom(p, fc.read()))
    #    print(len(path) * "---", f)

'''Build Homepage'''
with open(Path(public_path) / Path("index.html"), 'w') as index:
    index.write(t.index(pages))

'''Build Pages'''
#print(pages)

for page in pages:
    #print(Path(public_path) / Path(*page.path.parts[1:]).with_suffix(".html"))
    with open(Path(public_path) / Path(*page.path.parts[1:]).with_suffix(".html"), 'w') as target:
        target.write(t.page(page.text))
